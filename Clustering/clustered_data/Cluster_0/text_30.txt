Boyut İndirgeme
 Öznitelik seçimi Feature Selection
 Temel bileşenler analizi PCA
 Lineer Diskriminant Analizi
 TSNE
veride çok fazla değişkenin olması çoklu bağlantı problemi oluşturur çoklu bağlantı problemi bağımsız değişkenler arasında yüksek korelasyon olması durumudur 
Öznitelik seçimi dışındaki tüm yöntemler değişkenleri sıkıştırarak korelasyonsuz hale getirip kullanır
Bu yöntemler ne zaman kullanılamaz
boyut indirgeme yöntemleri değişkenler birbirine dik korelasyonsuz bileşenler iş tanımında model performansında önemli değişkenler ve görselleştirmeler isteniyorsa boyut indirgeme kullanmak doğru değildir

LDA
denetimli öğrenme uygulanacağında kullanılır çünkü boyut indirgeme hedef değişkene göre yapılır bu yöntemde hedef değişken göz önüne alınarak yüksek korelasyon tespit edilen değişkenler sadeleştirilir ve bilgi kaybı olmadan boyut indirgenir böylelikle çoklu bağlantı problemi ve aşırı öğrenme engellenir burada hedef değişkenin kullanılmasının sebebi LDA nın sınıflar arası mesafeyi maksimize edecek şekilde boyut indirgeme yapmasıdır

bunun için Değişkenler arası varyans hesaplanır daha değişkenler içi varyans hesaplanır ve son olarak hedef değişkenini göz önüne alarak değişkenler arası varyans maksimum değişken içi varyans minimum olacak şekilde yeniden ölçekleme yapar
NOT bu işlemden önce veriler standartlaştırılmalıdır


PCA toplam varyansı değiştirmeden birbiriyle yüksek korelasyonlu değişkenleri farklı kombinasyonda birbirine dik ve ilişkisiz hale getirir burada yapılan işlemde veri setinde bulunan değişkenler kaybolmaz sadece başka bir forma ölçeklendirilir ve veri kaybı olmaz 
NOT PCA aykırı gözlemlere ve standartlaştırılmamış değişkenlere karşı çok hassastır


PCA çeşitleri

Sparse PCA temel PCA yöntemi girdi değişkenlerinin çoğu sıfır olmayacak şekilde tasarlanmıştır veri seti çok fazla sıfır içeriyorsa SparsePCA kullanmak mantıklıdır
Randomize PCA Temel PCA dan çok farklı değildir büyük veri setlerinde maliyete sebep olacağı için kullanışlı olmayabilir
İncremental PCA veri seti belleğe sığmayacak büyüklülte olduğunda kullanılır veri gruplara ayrılarak işleme tabi tutulur
Kernel PCA doğrusal veri setlerinde uygulanan bir yöntemdir eğer veri setimiz doğrusal değilse ve boyut indirgeme yapmamız gerekiyorsa Kernel PCA kullanabiliriz

PCA ve LDA farkları

PCA denetimsiz bir algoritmadır ve veride varyansı maksimum yapan temel bileşenleri bulmaya çalışır
LDA denetimli algoritmadır ve sınıflar arasındaki farkı maksimize edecek şekilde bileşenler oluşturur 

PCA genellikle kümeleme problemlerinde LDA ise sınıflandırma problemlerinde kullanılır


TSNE
yüksek boyutlu verileri görselleştirmeyi ve boyut azaltmayı sağlayan yöntemdir bu yöntem veri setinin doğrusal olmadığı durumlarda kullanılan olasılıksal bir yöntemdir PCA varyansı en üst düzeye çıkarıp mesafeleri korumayı sağlar çünkü farklı olan şeylerin birbirlerine olan uzaklığı fazladır
ancak TSNE farklılıktan ziyade küçük ikili mesafeleri ve benzerlikleri koruyarak ilerler böylelikle değişkenler arası benzerlik ölçüsünü hesaplar ve bu ölçüleri optimize ederek bileşen oluşturur
NOT diğer yöntemlerde olduğu gibi veriler standartlaştırılmalıdır
NOT temeli olasılığa dayandığı için her zaman aynı sonucu vermez


Wed Dec    GMT GMT