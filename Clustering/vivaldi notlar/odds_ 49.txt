odds(otus/otsu): bir olayın gerçekleşme veya gerçekleşmeme ihtimali anlamına gelmektedir. 

Göreli risk (risk oranı veya kısaca "RO" olarak da bilinir), bir gruptaki (örneğin tedavi grubundaki) bir olayın riskinin, diğer gruptaki (örneğin tedavi görmeyen grup) olayın riskine olan oranıdır.

Otsu oranı (OR), bir grupta görülen bir olay oddsunun, diğer grupta görülen aynı olayın oddsuyla karşılaştırılmasıdır.

Tedavi ve netice arasında bir ilişki olmadığında, hem OR hem de RO, 1.0 olur, yani eşitlenirler

Bir tedavi ile bir netice arasında bir ilişki kurulduğunda, OR'deki ilişki tahmini biraz abartılıdır (RO, 1.0'dan daha uzaktır). Böylece, RO<1 ise, OR, RO'dan küçük olur (Tablo 3b'de bu durum görülmektedir); veya tam tersine, RO 1.0'dan büyük olduğunda, OR, RO'dan daha büyük olur (Tablo 3c–3e arasında bunlar görülüyor). Netice nadiren görüldüğünde (tipik olarak <%10), OR değeri RO değerinden çok farklı olmaz ve ikisi de, tedavi grubundaki riskin tedavi olmayanlara kıyasla daha az veya daha fazla olmasına bakılmaksızın birbirinin yerlerine kullanılabilirler
Sat Jan 08 2022 20:26:56 GMT+0300 (GMT+03:00)
https://evrimagaci.org/odds-vs-risk-istatistiki-analizde-sik-karsilasilan-tuzaklardan-biri-11182