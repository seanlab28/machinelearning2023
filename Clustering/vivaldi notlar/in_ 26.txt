  int a=5;               // initial value: 5
  int b(3);              // initial value: 3
  int c{2};              // initial value: 2
  int result;            // initial value undetermined

int foo = 0;
auto bar = foo;  // the same as: int bar = foo;
int foo = 0;
decltype(foo) bar;  // the same as: int bar;
[Kaynakça](https://cplusplus.com/doc/tutorial/variables/)
Mon Dec 19 2022 08:01:16 GMT+0300 (GMT+03:00)
