# ANOVA-F testi
* ikiden fazla kıyaslanacak grup olduğunda kullanılır
* Fisher geliştirmiştir
* sonuçlar F cetvel değeri ile karşılaştırılır
* teorik sürekli bir dağılımdır yani dağılımın fonksiyonu vardır
* dağılımın şekli iki adet serbestlik derecesine göre değişim göstermekle birlikte eğridir ve kuyruk kısmı sağ taraftadır
* serbestlik derecesi arttıkça dağılımın şekli simetrikleşir F dağılımına varyansların oranının dağılımı denir
## Varsayımları
* veriler normal dağılım göstermelidir
* hata terimleri normal dağılmalıdır
* faktör etkileri toplanabilir olmalıdır
* varyanslar homojen olmalıdır

Normallik varsayımı sağlanamadığında yapılacak olan F ve T testinin 1.tip hata yapma seviyeleri etkilenmektedir
Normallik varsayımının testinde en yaygın kullanılan testler
* kolmogorov-Smirnov
* Shapiro-Wilk 
testleridir

Normallik varsayımları tutmaz ise dönüştürme yapılmalıdır
sık kullanılan dönüştürmeler
* Logaritmik dönüştürme
* Karekök dönüştürme
* Arcsin dönüştürme

dönüştürmelerden sonra tekrar normallik varsayımı testleri kullanılmalıdır ona rağmen veri normal dağılıma dönüştürülemiyorsa non parametrik testler(verinin dağılımından bağımsız testler) kullanılmalıdır

varyansların homojenliği varsayımı 
çalışılan veri gruplarında varyansların homojenliği ön şartının sağlanıp sağlanmadığının test edilmesine yönelik değişik testler geliştirilmiştir bunlardan en çok kullanılanlar
* Bartlett
* Levene
* Cochran
* Hartley F Max
Varyansların homojen olup olmadığını test etmek için kullanılan pratik yöntem kıyaslamada kullanılan grupların en büyük varyansın en küçük varyansa bölümü 4 e eşit veya ondan küçük ise varyanslar homojen kabul edilir

Tek yönlü varyans analizi-One way ANOVA
* etkisi incelenmek istenen tek faktör olduğunda kullanılır
* gruplardaki denek sayılarından etkilenmez
* kullanımı basittir
* Diğer varyans analiz yöntemlerinin temelini oluşturur
Mon Feb 27 2023 20:29:14 GMT+0300 (GMT+03:00)
https://www.youtube.com/watch?v=IiGZNnXVfc4&t=390